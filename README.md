#Shadow Realms - Read Me File

Maybe you just became a developer for Shadow Realms, or maybe you need a refresher. Either way this guide should help you along the way.
Remember if you get stuck on anything you can always Contact the Developers team through our [Discord Server](https://discordapp.com/invite/013Szypt1oNEK7bV1)

***

# Guidelines for coding at Shadow Realms
If you are new to Git I suggest you read [this information](http://rogerdudler.github.io/git-guide/) before trying to code here.  We have listed some Git GUI Clients below for ease.

- [Github for Desktop](https://desktop.github.com/) *Windows/Mac*
- [SourceTree](https://www.sourcetreeapp.com/) *Windows/Mac*
- [SmartGit](https://www.syntevo.com/smartgit/) *Windows/Linux/Mac*
- [GitKraken](https://www.gitkraken.com/) *Windows/Linux/Mac*

### How to Use Git for Shadow Realms

To make everyone's life easier and not have to sift through merge conflicts, each person should know what they will work on before they start, you should also always Pull before you Push. The following list is how you should work on a file.

- **Pull Master Branch** *Pulling the Master makes sure your local code is up to date before you begin*
- **Create New Branch and Name it accordingly** *example: you are working on command /warp, you should name the branch **warp-%username%***
- **Commit for each section your working on** *Don't spam commits, but you should use commits often to show others what your changing*
- **Push your Created Branch** *This will upload it to the repository*
- **Create a Pull Request on BitBucket and wait for the Pull to be Approved** *make sure to delete each branch after they have been approved*